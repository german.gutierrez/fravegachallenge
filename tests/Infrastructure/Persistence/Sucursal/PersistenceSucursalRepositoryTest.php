<?php
declare(strict_types=1);

namespace Tests\Infrastructure\Persistence\Sucursal;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use App\Domain\Sucursal\Sucursal;
use App\Domain\Sucursal\SucursalNotFoundException;
use App\Infrastructure\Persistence\Sucursal\PersistenceSucursalRepository;
use Tests\TestCase;


class PersistenceSucursalRepositoryTest extends TestCase
{


    /**
     * @var \PDO
     */
    protected $pdo;
    protected $logger;

    
    public function __construct()
    {
        parent::__construct();
        $this->logger = new Logger('challengetest_logger');
        $this->logger->pushHandler(new StreamHandler('php://stdout', Logger::DEBUG));

        $container = $this->getAppInstance()->getContainer();
        $this->pdo = $container->get(\PDO::class);
        
    }

    protected function setUp(): void
    {
        $sql = "TRUNCATE TABLE `sucursales`";
        $statement = $this->pdo->prepare($sql); 
        $statement->execute();   

        $sucursalRepository = new PersistenceSucursalRepository($this->pdo, $this->logger);
        $sucursales = [
            ['jkhksjhdskaj',22,10],
            ['Italia 415, Vte. Lopez',-34.545748,-58.4981048],
            ['telecom estomba',-34.5638665,-58.4782597],
            ['Obelisco',-34.6037389,-58.3837591],
            
        ];
        foreach ($sucursales as $sucursal){
            $sucursalRepository->create($sucursal[0], $sucursal[1], $sucursal[2]);
        }
        
        
    }
    

    public function testFindAllSucursals()
    {
        
        $sucursales = [
            1 => new Sucursal(1,'jkhksjhdskaj',22,10),
            2 => new Sucursal(2,'Italia 415, Vte. Lopez',-34.545748,-58.4981048),
            3 => new Sucursal(3,'telecom estomba',-34.5638665,-58.4782597),
            4 => new Sucursal(4, 'Obelisco',-34.6037389,-58.3837591),            
        ];

        $sucursalRepository = new PersistenceSucursalRepository($this->pdo, $this->logger);

        $this->assertEquals(array_values($sucursales), $sucursalRepository->findAll());
    }
    
    public function testFindClosest()
    {
        $expected_sucursal1 = new Sucursal(2,'Italia 415, Vte. Lopez',-34.545748,-58.4981048);
        $expected_sucursal2 = new Sucursal(4, 'Obelisco',-34.6037389,-58.3837591);
        //4 => new Sucursal(4, 'Obelisco',-34.6037389,-58.3837591),            
        $closest_long_from_italia415 = -34.5458;
        $closest_lat_from_italia415 = -58.4981050;
        
        $closest_long_from_obelisco = -34.604;
        $closest_lat_from_obelisco = -58.384;
        
        
        $sucursalRepository = new PersistenceSucursalRepository($this->pdo, $this->logger);
        $this->assertEquals($expected_sucursal1, $sucursalRepository->findClosest($closest_long_from_italia415, $closest_lat_from_italia415));
        $this->assertEquals($expected_sucursal2, $sucursalRepository->findClosest($closest_long_from_obelisco, $closest_lat_from_obelisco));
    }

    public function testFindSucursalOfId()
    {
        $sucursal = new Sucursal(2,'Italia 415, Vte. Lopez',-34.545748,-58.4981048);

        
        $sucursalRepository = new PersistenceSucursalRepository($this->pdo, $this->logger);
        $this->assertEquals($sucursal, $sucursalRepository->findSucursalOfId(2));
    }

    public function testFindSucursalOfIdThrowsNotFoundException()
    {
        $sucursalRepository = new PersistenceSucursalRepository($this->pdo, $this->logger);
        $this->expectException(SucursalNotFoundException::class);
        $sucursalRepository->findSucursalOfId(999);
    }
     
}
