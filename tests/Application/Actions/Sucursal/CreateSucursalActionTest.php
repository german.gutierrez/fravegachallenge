<?php
declare(strict_types=1);

namespace Tests\Application\Actions\Sucursal;

use App\Application\Actions\ActionError;
use App\Application\Actions\ActionPayload;
use App\Application\Handlers\HttpErrorHandler;
use App\Domain\Sucursal\Sucursal;
use App\Domain\Sucursal\SucursalCreateException;
use App\Domain\Sucursal\SucursalLocationOutOfBoundsException;
use App\Domain\Sucursal\SucursalNotFoundException;
use App\Domain\Sucursal\SucursalRepository;
use Slim\Psr7\Factory\StreamFactory;
use DI\Container;
use Slim\Middleware\ErrorMiddleware;
use Tests\TestCase;

class CreateSucursalActionTest extends TestCase
{
    
    public function testAction()
    {
        $app = $this->getAppInstance();

        /** @var Container $container */
        $container = $app->getContainer();
        $pdo = $container->get(\PDO::class);
        $sql = "TRUNCATE TABLE `sucursales`";
        $statement = $pdo->prepare($sql); 
        $statement->execute();  

        
        $json = '{"direccion":"CREATE TEST","longitud":-66,"latitud":-66}';
        
        $custom_stream = (new StreamFactory())->createStream($json );
        
        $request = $this->createRequest('POST', '/sucursal/',['HTTP_ACCEPT' => 'application/json'], [], [],$custom_stream);
        
        $response = $app->handle($request);

        $payload = (string) $response->getBody();
        
        $expectedPayload = new ActionPayload(200, Sucursal::SUCURSAL_CREATED);
        $serializedPayload = json_encode($expectedPayload, JSON_PRETTY_PRINT);

        $this->assertEquals($serializedPayload, $payload, "AAA");
    }

   public function testActionThrowsSucursalLocationOutOfBoundsException()
    {
        $app = $this->getAppInstance();

        $callableResolver = $app->getCallableResolver();
        $responseFactory = $app->getResponseFactory();

        $errorHandler = new HttpErrorHandler($callableResolver, $responseFactory);
        $errorMiddleware = new ErrorMiddleware($callableResolver, $responseFactory, true, false ,false);
        $errorMiddleware->setDefaultErrorHandler($errorHandler);

        $app->add($errorMiddleware);

        /** @var Container $container */
        $container = $app->getContainer();
        // latitud esta por fuera del rango -90 90
        $json = '{"direccion":"CREATE TEST","longitud":-66,"latitud":-96}'; 
        
        $custom_stream = (new StreamFactory())->createStream($json );
        
        $request = $this->createRequest('POST', '/sucursal/',['HTTP_ACCEPT' => 'application/json'], [], [],$custom_stream);
        
        $response = $app->handle($request);

        $payload = (string) $response->getBody();
        $expectedError = new ActionError(ActionError::SERVER_ERROR, Sucursal::SUCURSAL_OUTOFBOUNDS_ERROR);
        $expectedPayload = new ActionPayload(500, null, $expectedError);
        $serializedPayload = json_encode($expectedPayload, JSON_PRETTY_PRINT);
        
        $this->assertEquals($serializedPayload, $payload);
    }
    
    public function testActionThrowsSucursalCreateException()
    {
        $app = $this->getAppInstance();

        $callableResolver = $app->getCallableResolver();
        $responseFactory = $app->getResponseFactory();

        $errorHandler = new HttpErrorHandler($callableResolver, $responseFactory);
        $errorMiddleware = new ErrorMiddleware($callableResolver, $responseFactory, true, false ,false);
        $errorMiddleware->setDefaultErrorHandler($errorHandler);

        $app->add($errorMiddleware);

        /** @var Container $container */
        $container = $app->getContainer();

         // latitud esta por fuera del rango -90 90
        $json = '{"direccion":"CREATE TEST","longitud":-66,"latitud":-66}'; 
        
        $custom_stream = (new StreamFactory())->createStream($json );
        
        $request = $this->createRequest('POST', '/sucursal/',['HTTP_ACCEPT' => 'application/json'], [], [],$custom_stream);
        
        $response = $app->handle($request);

        $payload = (string) $response->getBody();
        $expectedError = new ActionError(ActionError::SERVER_ERROR,Sucursal::SUCURSAL_DIRECCION_EXIST );
        $expectedPayload = new ActionPayload(500, null, $expectedError);
        $serializedPayload = json_encode($expectedPayload, JSON_PRETTY_PRINT);

        $this->assertEquals($serializedPayload, $payload);
    }
}
