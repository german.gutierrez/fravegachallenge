<?php
declare(strict_types=1);

namespace Tests\Domain\Sucursal;

use App\Domain\Sucursal\Sucursal;
use Tests\TestCase;

class SucursalTest extends TestCase
{
    public function sucursalProvider()
    {
        return [
            [1,'jkhksjhdskaj',22,10],
            [2,'Italia 415, Vte. Lopez',-34.545748,-58.4981048],
            [3,'telecom estomba',-34.5638665,-58.4782597],
            [4, 'Obelisco',-34.6037389,-58.3837591],
            
        ];
    }

    /**
     * @dataProvider sucursalProvider
     * @param int    $id
     * @param string $direccion
     * @param string $longitud
     * @param string $latitud
     */
    public function testGetters(int $id, string $direccion, float $longitud, float $latitud)
    {
        $sucursal = new Sucursal($id, $direccion, $longitud, $latitud);

        $this->assertEquals($id, $sucursal->getId());
        $this->assertEquals($direccion, $sucursal->getDireccion());
        $this->assertEquals($longitud, $sucursal->getLongitud());
        $this->assertEquals($latitud, $sucursal->getLatitud());
    }

    /**
     * @dataProvider sucursalProvider
     * @param int    $id
     * @param string $direccion
     * @param string $longitud
     * @param string $latitud
     */
    public function testJsonSerialize(int $id, string $direccion, float $longitud, float $latitud)
    {
        $sucursal = new Sucursal($id, $direccion, $longitud, $latitud);

        $expectedPayload = json_encode([
            'id' => $id,
            'direccion' => $direccion,
            'longitud' => $longitud,
            'latitud' => $latitud,
        ]);

        $this->assertEquals($expectedPayload, json_encode($sucursal));
    }
}
