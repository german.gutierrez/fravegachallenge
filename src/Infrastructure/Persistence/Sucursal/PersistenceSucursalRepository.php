<?php
declare(strict_types=1);

namespace App\Infrastructure\Persistence\Sucursal;
use App\Domain\Sucursal\Sucursal;
use App\Domain\Sucursal\SucursalNotFoundException;
use App\Domain\Sucursal\SucursalRepository;
use App\Domain\Sucursal\SucursalLocationOutOfBoundsException;
use App\Domain\Sucursal\SucursalDireccionAlreadyExistException;
use App\Domain\Sucursal\HealthCheckException;
use Psr\Log\LoggerInterface;

class PersistenceSucursalRepository implements SucursalRepository
{
     private $dbConnection;
     private $logger;
    
    public function __construct(?\PDO $dbConnection, LoggerInterface $logger)
    {
        $this->dbConnection = $dbConnection;
        $this->logger = $logger;
    }
    
    
    public function getCon(){
        if(!$this->checkhealth()){
            throw new HealthCheckException();
        }
        return $this->dbConnection;
    }

    /**
     * {@inheritdoc}
     */
    public function findAll(): array
    {
        $stm = $this->getCon()->query("SELECT id, direccion, "
                . "X(location) as longitud, Y(location) as latitud "
                . "FROM sucursales");
        $rows = [];
        
        foreach( $stm->fetchAll() as $item){
            $rows[]=new Sucursal($item['id'], 
                    $item['direccion'], 
                    $item['longitud'], 
                    $item['latitud']);
        }
        
        return array_values($rows);
    }



    /**
     * {@inheritdoc}
     */
    public function findSucursalOfId(int $id): Sucursal
    {
        $stmt = $this->getCon()
             ->prepare("SELECT id, direccion, X(location) as longitud, "
                     . "Y(location) as latitud FROM sucursales where id = ?");
        $stmt->execute(array($id));
        $item = $stmt->fetch();
        
        if (!$item) {
            throw new SucursalNotFoundException();
        }
       //$this->logger->info("Sucursal ", [$item]);
        return new Sucursal($item['id'], 
                $item['direccion'], 
                $item['longitud'], 
                $item['latitud']);
    }

    /**
     * {@inheritdoc}
     */
    public function create(string $direccion, float $longitud, float $latitud): bool {
        
        
        if(Sucursal::isNotValidSucursalLongitude($longitud) 
                || Sucursal::isNotValidSucursalLatitude($latitud)) {
            
            throw new SucursalLocationOutOfBoundsException();
        }
            
        $ahora = date("Y-m-d H:i:s");
        $stmt = $this->getCon()->prepare("INSERT INTO sucursales "
                . "(direccion, location, created_at) "
                . "VALUES (:direccion,POINT(:longitud, :latitud), :fecha);");
        $stmt->bindParam(':direccion', $direccion);
        $stmt->bindParam(':longitud', $longitud);
        $stmt->bindParam(':latitud', $latitud);
        $stmt->bindParam(':fecha', $ahora);
        try{
            return $stmt->execute();        
        } catch(\PDOException  $e){
            throw new SucursalDireccionAlreadyExistException();
        }
            
        
       
    }

    /**
     * {@inheritdoc}
     */
    public function findClosest(float $longitud, float $latitud): Sucursal {
        
        $stmt = $this->getCon()
                ->prepare("select id, direccion, X(location) as longitud, "
                        . "Y(location) as latitud , "
                        . "ST_Distance(location, Point(?,?)) as distancia "
                        . "from sucursales order by distancia limit 1;");
        $stmt->execute(array($longitud, $latitud));
        $item = $stmt->fetch();
        if (!$item) {
            throw new SucursalesEmptyException();
        }
        

        return new Sucursal($item['id'], 
                $item['direccion'], 
                $item['longitud'], 
                $item['latitud']);
    }

    public function checkhealth(): bool {
        return $this->dbConnection instanceof \PDO;
    }
    

}
