<?php
declare(strict_types=1);

namespace App\Application\Actions\Sucursal;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class ListSucursalesAction extends SucursalAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $sucursales = $this->sucursalRepository->findAll();

        $this->logger->info("Listado de surcursales leido.",$sucursales);

        return $this->respondWithData($sucursales);
    }
}
