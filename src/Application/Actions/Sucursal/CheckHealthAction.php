<?php
declare(strict_types=1);

namespace App\Application\Actions\Sucursal;

use Psr\Http\Message\{ResponseInterface as Response, ServerRequestInterface as Request};
use Fig\Http\Message\StatusCodeInterface;
class CheckHealthAction extends SucursalAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
    
       
       $health = $this->sucursalRepository->checkhealth();
       
       
       $this->logger->info("healthcheck",[$health]);    

        return $this->respondWithData( $health ? "true": "false"  ,$health ? StatusCodeInterface::STATUS_OK: StatusCodeInterface::STATUS_SERVICE_UNAVAILABLE);
    }
}
