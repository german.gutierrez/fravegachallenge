<?php
declare(strict_types=1);

namespace App\Domain\Sucursal;

use App\Domain\DomainException\DomainException;

class SucursalDireccionAlreadyExistException extends DomainException
{
    public $message = Sucursal::SUCURSAL_DIRECCION_EXIST;
}

