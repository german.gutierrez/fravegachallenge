<?php
declare(strict_types=1);

namespace App\Domain\Sucursal;

interface SucursalRepository
{
    /**
     * @return Sucursal[]
     */
    public function findAll(): array;

    /**
     * @param int $id
     * @return Sucursal
     * @throws SucursalNotFoundException
     */
    public function findSucursalOfId(int $id): Sucursal;
    
    /**
     * 
     * @param string $dreccion
     * @param float $longitud
     * @param float $latitud
     * @return bool
     * @throws SucursalLocationOutOfBoundsException
     * @throws SucursalDireccionAlreadyExistException
     */
    public function create(string $dreccion, float $longitud, float $latitud): bool;
    
    
    /**
     * Devuelve la sucursal mas cercana a la cordenadas
     * 
     * @param float $longitud
     * @param float $latitud
     * @return \App\Domain\Sucursal\Sucursal
     * @throws SucursalesEmptyException
     */
    public function findClosest(float $longitud, float $latitud): Sucursal;


    /**
     * checks database connection is established
     * @return bool
     */  
    public function checkhealth(): bool;
}
