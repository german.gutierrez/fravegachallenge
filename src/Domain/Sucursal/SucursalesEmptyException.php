<?php
declare(strict_types=1);

namespace App\Domain\Sucursal;

use App\Domain\DomainException\DomainRecordNotFoundException;

class SucursalesEmptyException extends DomainRecordNotFoundException
{
    public $message = 'Aun no hay sucursales en la base de datos';
}
