<?php
declare(strict_types=1);
namespace App\Domain\Sucursal;

use App\Domain\DomainException\DomainException;

/**
 * Description of SucursalLocationOutOfBoundsException
 *
 * @author Germán Gutierrez <german.gutierrez@snoopconsulting.com>
 */
class HealthCheckException extends DomainException
{
    public $message = "No se puede establecer conexión con la base de datos ";
}
