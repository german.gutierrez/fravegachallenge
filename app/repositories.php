<?php
declare(strict_types=1);

use App\Domain\Sucursal\SucursalRepository;
use App\Infrastructure\Persistence\Sucursal\PersistenceSucursalRepository;
use DI\ContainerBuilder;

return function (ContainerBuilder $containerBuilder) {
    $containerBuilder->addDefinitions([
        SucursalRepository::class => \DI\autowire(PersistenceSucursalRepository::class),
    ]);
};
