<?php
declare(strict_types=1);

use App\Application\Actions\Sucursal\{ListSucursalesAction, ViewSucursalAction};
use App\Application\Actions\Sucursal\{CreateSucursalAction, GetNearSucursalAction, CheckHealthAction};
use Psr\Http\Message\{ResponseInterface as Response, ServerRequestInterface as Request };
use Slim\App;
use Slim\Interfaces\RouteCollectorProxyInterface as Group;

return function (App $app) {
    $app->options('/{routes:.*}', function (Request $request, Response $response) {
        // CORS Pre-Flight OPTIONS Request Handler
        return $response;
    });

    $app->get('/', function (Request $request, Response $response) {
        $response->getBody()->write('Hello world!');
        return $response;
    });
    
    $app->get('/healthcheck', CheckHealthAction::class);
    
    $app->group('/sucursal', function (Group $group) {
        
        $group->post('/', CreateSucursalAction::class);
        $group->get('/', ListSucursalesAction::class);
        //permitimos sin la barra
        $group->post('', CreateSucursalAction::class);
        $group->get('', ListSucursalesAction::class);
        
        $group->post('/getnear', GetNearSucursalAction::class);
        $group->get('/{id}', ViewSucursalAction::class);
        
    });
};
