<?php
declare(strict_types=1);

use DI\ContainerBuilder;
use Monolog\Logger;

return function (ContainerBuilder $containerBuilder) {
    $dotenv = Dotenv\Dotenv::createUnsafeImmutable(dirname(__DIR__)); 
    $dotenv->load();

    // Global Settings Object
    $containerBuilder->addDefinitions([
        'settings' => [
            'displayErrorDetails' => true, // falso si va a prod
            'logger' => [
                'name' => 'challenge',
                'path' => 'php://stdout',
                'level' => Logger::DEBUG,
            ],
            'db' => [
                'host' => DI\env('DB_HOST'),
                'user' => DI\env('DB_USERNAME'),
                'password' => DI\env('DB_PASSWORD'),
                'dbname' => DI\env('DB_DATABASE'),
                'port' => DI\env('DB_PORT'),
            ],
        ],
    ]);
};
