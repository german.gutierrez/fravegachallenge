CREATE DATABASE fravegatest CHARACTER SET utf8 COLLATE utf8_general_ci;
CREATE USER fravegatest@'%' IDENTIFIED BY 'fravegatest';
GRANT ALL PRIVILEGES ON fravegatest.* TO 'fravegatest'@'%';
